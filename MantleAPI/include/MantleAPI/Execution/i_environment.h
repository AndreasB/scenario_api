/*******************************************************************************
 * Copyright (c) 2021-2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  i_environment.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_EXECUTION_IENVIRONMENT_H
#define MANTLEAPI_EXECUTION_IENVIRONMENT_H

#include <MantleAPI/Common/i_geometry_helper.h>
#include <MantleAPI/Common/time_utils.h>
#include <MantleAPI/EnvironmentalConditions/road_condition.h>
#include <MantleAPI/EnvironmentalConditions/weather.h>
#include <MantleAPI/Map/i_coord_converter.h>
#include <MantleAPI/Map/i_lane_location_query_service.h>
#include <MantleAPI/Map/map_details.h>
#include <MantleAPI/Traffic/i_entity_repository.h>
#include <MantleAPI/Traffic/i_controller_repository.h>

#include <string>

namespace mantle_api
{
class IEnvironment
{
public:
  virtual ~IEnvironment() = default;

  /// Load a map file and parse it into the memory.
  ///
  /// @param file_path map file path from the scenario file. If this path is not resolved by the engine, the
  ///                  environment must do so.
  virtual void CreateMap(const std::string& map_file_path, const mantle_api::MapDetails& map_details) = 0;

  /// Assigns an entity to a copy of the specified controller. This controller needs to be created beforehand. Only
  /// one controller can be added to an entity
  ///
  /// @param entity The entity to be manipulated by the specified controller.
  /// @param controller_id Identifies the controller to manipulate the entity.
  virtual void AddEntityToController(IEntity& entity, UniqueId controller_id) = 0;

  virtual void RemoveControllerFromEntity(UniqueId entity_id) = 0;

  /// Updates the control strategies for an entity.
  ///
  /// @param entity_id          Specifies the entity to be updated
  /// @param control_strategies Specifies the desired movement behaviour for the entity
  virtual void UpdateControlStrategies(
      UniqueId entity_id, std::vector<std::shared_ptr<mantle_api::ControlStrategy>> control_strategies) = 0;

  /// Checks, if a control strategy of a certain type for a specific entity has been fulfilled
  ///
  /// @param entity_id    The entity to check
  /// @param type         The control strategy type
  virtual bool HasControlStrategyGoalBeenReached(UniqueId entity_id, mantle_api::ControlStrategyType type) const = 0;

  virtual const ILaneLocationQueryService& GetQueryService() const = 0;
  virtual const ICoordConverter* GetConverter() const = 0;
  virtual const IGeometryHelper* GetGeometryHelper() const = 0;

  virtual IEntityRepository& GetEntityRepository() = 0;

  virtual IControllerRepository& GetControllerRepository() = 0;

  /// @brief DateTime in UTC (converted from RFC 3339 standard)
  virtual void SetDateTime(mantle_api::Time time) = 0;
  virtual mantle_api::Time GetDateTime() = 0;

  /// @brief Time since start of simulation
  virtual mantle_api::Time GetSimulationTime() = 0;

  virtual void SetWeather(Weather weather) = 0;
  virtual void SetRoadCondition(std::vector<FrictionPatch> friction_patches) = 0;
  virtual void SetTrafficSignalState(const std::string& traffic_signal_name, const std::string& traffic_signal_state) = 0;

  // Execute a command that is specific for an environment implementation
  ///
  /// @param actors    the actors (if any) for which a command is executed
  /// @param type      type of the command
  /// @param command   custom payload
  virtual void ExecuteCustomCommand(const std::vector<std::string>& actors, const std::string& type, const std::string& command) = 0;
};
}  // namespace mantle_api

#endif  // MANTLEAPI_EXECUTION_IENVIRONMENT_H
