/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  interface_test.cpp */
//-----------------------------------------------------------------------------

#include "MantleAPI/Test/test_utils.h"

TEST(InterfaceTest, GivenTeleportAction_When_ThenHostVehicleIsPlaced)
{
  mantle_api::Position inert_pos{};
  inert_pos = mantle_api::OpenDrivePosition{{0, 0}, units::length::meter_t{0}, units::length::meter_t{0}};
  mantle_api::MockEnvironment env{};
  env.CreateMap("dummy_map_path", {});

  mantle_api::VehicleProperties vehicle_properties;
  vehicle_properties.is_host = true;
  vehicle_properties.model = "G12";

  auto& repo = env.GetEntityRepository();
  auto& host_vehicle = repo.Create(0, "host", vehicle_properties);
  const auto* const converter = env.GetConverter();
  auto world_pos = converter->Convert(inert_pos);
  host_vehicle.SetPosition(world_pos);
  host_vehicle.SetVisibility(mantle_api::EntityVisibilityConfig{true, false, true, {"radar"}});
}
