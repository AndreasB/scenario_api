# Scenario API

A collection of interfaces for abstraction between a scenario engine and an environment simulator. 
It is intended to be usable with a wide variety of scenario description languages by implementing according scenario engines. 

Remark: This is currently work in progress and no stable state is reached yet. 

## Used libraries

### Units
License: MIT License
URL: https://github.com/nholthaus/units
Version: v2.3.1
